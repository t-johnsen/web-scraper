# NodeJS - Web scraper
A simple application which scrapes IMDB's Top 250 Movie titles and writes content to file.

## Getting started and usage
1. Clone repo to a local directory.
2. Open the project in prefered IDE/code editor in root folder.
3. Run ```npm install``` from root.
4. Run the app.js file with node runtime from the console: ```node app.js```

## Prerequisites
* [NodeJS]
* [Puppeteer]

[//]: #
[NodeJS]: <https://nodejs.org/en/>
[Puppeteer]: <https://github.com/puppeteer/puppeteer>
