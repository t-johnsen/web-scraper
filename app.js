const fs = require('fs');
const puppeteer = require('puppeteer');

async function readPage (){

    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();

    await page.goto('https://www.imdb.com/chart/top/', {waitUntil: 'networkidle2'});

    let list = await page.evaluate(() => {
        let nodes = document.querySelectorAll('tbody[class="lister-list"]  > tr');
        let data = Array.from(nodes).map(e => {return e});
        let titles = data.map(e => {return 'Rank & title: ' + e.querySelector('td[class="titleColumn"]').innerText + ' | IMDB Rating: ' +  e.querySelector('td[class="ratingColumn imdbRating"] strong').innerText});

        
        return titles;
    })
    
    console.log(list);

    await browser.close();

    fs.writeFile('IMDBTop250Movies.txt', JSON.stringify(list), err => err ? console.log(err) : null);
}

readPage();
